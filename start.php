<?php
#
# dmulator
#
# Copyright © 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

include(dirname(__FILE__) . "/DMSystem.php");

function cdmEmuAuthenticate() {
	header('WWW-Authenticate: Basic realm="ByPassword"');
	header('HTTP/1.0 401 Unauthorized');
	die('<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>401 Authorization Required</title>
</head><body>
<h1>Authorization Required</h1>
<p>This server could not verify that you
are authorized to access the document
requested.  Either you supplied the wrong
credentials (e.g., bad password), or your
browser doesn\'t understand how to supply
the credentials required.</p>
</body></html>');
}

$ok = false;
foreach (DmuConfig::$users as $user => $password) {
	if (array_key_exists("PHP_AUTH_USER", $_SERVER)
			&& array_key_exists("PHP_AUTH_PW", $_SERVER)
			&& $user == $_SERVER['PHP_AUTH_USER']
			&& $password == $_SERVER['PHP_AUTH_PW']) {
		setcookie("DMID", "sessionZ" . $user . ">AXU5UJ3UAKUA6J29Y4NQ", 0, "/");
		$ok = true;
		die("<!DOCTYPE html><html><head></head><body>Login complete.</body></html>");
	}
}

if (!isset($_SERVER['PHP_AUTH_USER']) or !$ok) {
	cdmEmuAuthenticate();
}

