<?php
#
# dmulator
#
# Copyright © 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

require_once(dirname(__FILE__) . '/config.php');

spl_autoload_register('dmEmuAutoloader');

function dmEmuAutoloader($class) {
	// We may be using this script in a context in which there are other
	// autoloaders defined, so don't complain if we can't find something.
	$path = dirname(__FILE__) . "/classes/" . $class . ".class.php";
	if (file_exists($path)) {
		include_once($path);
	}
}

require_once(dirname(__FILE__) . "/api_functions/functions.php");

