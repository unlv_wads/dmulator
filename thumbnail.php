<?php
#
# dmulator
#
# Copyright © 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

include(dirname(__FILE__) . "/DMSystem.php");

if (empty($_GET['CISOROOT'])) {
	die("No item specified.");
}

$ptr = abs((int) substr($_GET['CISOPTR'], 0, 6));
$alias = substr($_GET['CISOROOT'], 0, 50);

if ($alias == "/dynamic") {
	$path = sprintf("%s/sample_data/dynamic/dynamic_thumb.jpg",
			dirname(__FILE__));
} else {
	$path = sprintf("%s/sample_data%s/image/icon%d.jpg",
			dirname(__FILE__),
			$alias,
			$ptr + 1);
}

header("Content-Type: image/jpeg");

die(file_get_contents($path));
