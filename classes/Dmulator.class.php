<?php
#
# dmulator
#
# Copyright © 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

/**
 * For instructions on using, see README.txt.
 *
 * @author Alex Dolski <alex.dolski@unlv.edu>
 * @license http://www.opensource.org/licenses/mit-license.php
 */
class Dmulator {

	private static $available_versions = array(5, 6);
	private static $domxml = array(
		'collections' => null,
		'fields' => null,
		'objects' => null,
		'vocabulary' => null
	);
	private static $instance;

	private $emulate_version = 6;


	/**
	 * @return Dmulator
	 */
	public static function getInstance() {
		if (!self::$instance instanceof self) {
			self::$instance = new self;
		}
		return self::$instance;
	}

	/**
	 * @param string alias
	 * @return Boolean
	 */
	public static function collectionExists($alias) {
		$dxml = self::getCollectionsDOMXML();
		$dxp = new DOMXPath($dxml);
		$result = $dxp->query(
			sprintf('//collection[@alias = "%s"]', $alias));
		return (bool) $result->length;
	}

	/**
	 * @return array
	 */
	public static function getFieldNicks() {
		$dxml = self::getFieldsDOMXML();
		$dxp = new DOMXPath($dxml);
		$nicks = array();
		foreach ($dxp->query('//fieldDefs/fields/field/@nick') as $node) {
			$nicks[] = $node->value;
		}
		return $nicks;
	}

	/**
	 * @return DOMDocument
	 */
	public static function getCollectionsDOMXML() {
		return self::getDOMXML('collections');
	}

	/**
	 * @return DOMDocument
	 */
	public static function getFieldsDOMXML() {
		return self::getDOMXML('fields');
	}

	/**
	 * @return DOMDocument
	 */
	public static function getObjectsDOMXML() {
		return self::getDOMXML('objects');
	}

	/**
	 * @return DOMDocument object
	 */
	public static function getVocabularyDOMXML() {
		return self::getDOMXML('vocabulary');
	}

	private static function getDOMXML($which_one) {
		if (!self::$domxml[$which_one] instanceof DOMDocument) {
			self::$domxml[$which_one] = new DOMDocument(
				"1.0", self::getInstance()->getEncoding());
			self::$domxml[$which_one]->preserveWhiteSpace = false;
			self::$domxml[$which_one]->load(
				dirname(__FILE__) . DmuConfig::METADATA_PATH . "/"
					. $which_one . ".xml");
		}
		return self::$domxml[$which_one];
	}

	/**
	 * @param string alias
	 * @param int ptr
	 * @return string
	 */
	public static function getFilenameByAliasPtr($alias, $ptr) {
		if ($alias == "/dynamic") {
			return "dynamic.jpg";
		} else {
			$dxml = self::getObjectsDOMXML();
			$dxp = new DOMXPath($dxml);
			$result = $dxp->query(
				sprintf('//xml[@ptr = %d and @alias = "%s"]/find',
						$ptr, $alias));
			return $result->item(0) ? $result->item(0)->nodeValue : "";
		}
	}

	/**
	 * @param string str
	 * @return string
	 * @since 0.1
	 */
	public static function xmlentities($str) {
		return str_replace(array('&', '"', "'", '<', '>'),
			array('&amp;', '&quot;', '&apos;', '&lt;', '&gt;'),
			$str);
	}

	protected function __construct() {}

	/**
	 * Returns either 'utf-8' or 'us-ascii' depending on the version being
	 * emulated. It is not possible to set the encoding independently.
	 *
	 * @return string
	 * @todo Eliminate this
	 * @deprecated
	 */
	public function getEncoding() {
		return ($this->emulate_version >= 5) ? "utf-8" : "us-ascii";
	}

	/**
	 * @return int
	 */
	public function getVersionToEmulate() {
		return $this->emulate_version;
	}

	/**
	 * @param int version
	 * @throws Exception
	 */
	public function setVersionToEmulate($version) {
		if (!in_array($version, self::$available_versions)) {
			throw new Exception("Version to emulate must be one of: "
				. implode(", ", self::$available_versions));
		}
		$this->emulate_version = $version;
	}

}

