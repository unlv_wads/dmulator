<?php
#
# dmulator
#
# Copyright © 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

include(dirname(__FILE__) . "/DMSystem.php");

if (empty($_GET['CISOROOT'])) {
	die("No item specified.");
}

$ptr = abs((int) substr($_GET['CISOPTR'], 0, 6));
$alias = substr($_GET['CISOROOT'], 0, 50);

$filename = Dmulator::getFilenameByAliasPtr($alias, $ptr);

// I have no idea how showfile handles the Content-Type header. I guess check
// for a pdf extension and hope for the best.
$mime = "application/octet-stream";
switch (substr($filename, -4, 4)) {
	case ".pdf":
		$mime = "application/pdf";
		break;
}

if ($alias != "/dynamic") {
	$path = sprintf("%s/sample_data%s/image/%s",
			dirname(__FILE__), $alias, $filename);
} else {
	$path = sprintf("%s/sample_data%s",
			dirname(__FILE__), $filename);
}

header("Content-Type: " . $mime);

readfile($path);
