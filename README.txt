Dmulator provides a platform on which applications interfacing with the
CONTENTdm® PHP API can be run and tested without the need for a test server
running the real version of CONTENTdm®.

Dmulator is not a comprehensive emulator, but it tackles the "important parts"
for the purposes of custom application development, including DMSystem.php
(containing the PHP API), getimage.exe, login.exe, showfile.exe, showpdf.exe,
and thumbnail.exe. All PHP API functions are emulated except the favorites
functions. Emulation accuracy is pretty good and is kept in check by a
comprehensive PHPUnit test suite.

Dmulator is somewhat more flexible than the real thing in that different
versions of the API (and therefore, different versions of CONTENTdm®) can be
emulated without the need for separate test servers and all of the logistic,
licensing, and installation hurdles that would entail. It also enables
previously impossible workflows - like sandbox/staging/production - by
enabling CONTENTdm® applications to run in the sandboxes, which themselves may
be running any OS supported by PHP. It does not require any special PHP
modules and can therefore run on PHP versions that CONTENTdm® cannot. The
sample data is customizable and can serve as a stable base on which to perform
automated testing. Finally, it's easy to install and use.

Dmulator is not intended to be a CONTENTdm® replacement, is not even close to
being one, and never will be one. Its main purpose is to support
dmBridge (http://digital.library.unlv.edu/software/dmbridge) development.

Dmulator uses no OCLC source code.