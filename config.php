<?php

abstract class DmuConfig {

	/**
	 * Shouldn't be necessary to change this
	 */
	const METADATA_PATH = "/../sample_metadata";

	/**
	 * Used for start.exe emulation by start.php. Additional users can be added
	 * as username => password pairs.
	 */
	public static $users = array(
		'admin' => 'admin',
		'testuser' => 'password'
	);

}

