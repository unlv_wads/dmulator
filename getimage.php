<?php
#
# dmulator
#
# Copyright © 2011 Board of Regents of the Nevada System of Higher
# Education, on behalf of the University of Nevada, Las Vegas
#

include(dirname(__FILE__) . "/DMSystem.php");

if (empty($_GET['CISOROOT'])) {
	die("Error looking up collection in catalog.");
}

$width = abs((int) substr($_GET['DMWIDTH'], 0, 4));
$height = abs((int) substr($_GET['DMHEIGHT'], 0, 4));
$ptr = abs((int) substr($_GET['CISOPTR'], 0, 6));
$alias = substr($_GET['CISOROOT'], 0, 50);

$filename = Dmulator::getFilenameByAliasPtr($alias, $ptr);

$image = new SimpleImage();
if ($alias == "/dynamic") {
	$path = sprintf("%s/sample_data/dynamic/%s",
			dirname(__FILE__), $filename);
} else {
	$path = sprintf("%s/sample_data%s/image/%s",
			dirname(__FILE__), $alias, $filename);
}
$image->load($path);

if ($width && $height) {
	if ($image->getWidth() > $image->getHeight()) {
		$image->resizeToWidth($width);
	} else {
		$image->resizeToHeight($height);
	}
}

header("Content-Type: image/jpeg");

$image->output();
